import cv2
from PIL import Image
from torch.nn.functional import interpolate
from torchvision.transforms import ToTensor

from utils.utils import torch, non_max_suppression


def _extract_class_names(class_names, img_scale, outputs):
    objects = []
    for i, output in enumerate(outputs):
        item = [class_names[int(output[-1])], float(output[4])]
        box = [int(value * img_scale[i]) for i, value in enumerate(output[:4])]
        x1, y1, x2, y2 = box
        x = int((x2 + x1) / 2)
        y = int((y1 + y2) / 2)
        w = x2 - x1
        h = y2 - y1
        item.append([x, y, w, h])
        objects.append(item)
    return objects


def _resize(image, size):
    image = interpolate(image.unsqueeze(0), size=size, mode="nearest").squeeze(0)
    return image


def yolo_prediction(model, device, image, class_names):
    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    images = ToTensor()(Image.fromarray(image))
    _, h, w = images.shape
    img_scale = [w / 416, h / 416, w / 416, h / 416]

    model.eval()
    with torch.no_grad():
        outputs = model(_resize(images, 416).unsqueeze(0).to(device))
        if outputs[0] is None:
            return []
        outputs = non_max_suppression(outputs, conf_thres=0.5, nms_thres=0.40)
        if outputs[0] is None:
            return []

    return _extract_class_names(class_names, img_scale, outputs[0].cpu().data)
