import os

executable = (
    "C:\\Users\\mcfle\\PycharmProjects\\vehicle_counting_test_one\\"
    "ffmpeg-2020-09-30-git-9d8f9b2e40-full_build\\bin\\ffmpeg.exe"
)


def compress_mp4(path_to_filename: str) -> str:
    new_path_to_filename = "".join(path_to_filename.split(".")[:-1]) + "_compressed.mp4"
    if not os.path.isfile(new_path_to_filename):
        os.system(f"ffmpeg -i {path_to_filename} -vcodec h264  -crf 28 -strict -2  {new_path_to_filename}")
        return new_path_to_filename
    else:
        raise FileExistsError(f"{new_path_to_filename} exists already")


def main():
    path_to_file = "video_files_to_compress"
    output_paths = []
    files_to_convert = os.listdir(path_to_file)
    for file in files_to_convert:
        if file.endswith("mp4"):
            file = os.path.abspath(os.path.join(path_to_file, file))
            new_name = compress_mp4(os.path.abspath(file))
            print("successfully compressed {0} to {1}".format(file, new_name))
            output_paths.append(new_name)


if __name__ == "__main__":
    main()
