import os

import ffmpy

executable = (
    "C:\\Users\\mcfle\\PycharmProjects\\vehicle_counting_test_one\\"
    "ffmpeg-2020-09-30-git-9d8f9b2e40-full_build\\bin\\ffmpeg.exe"
)


def convert_from_mts_to_avi(path_to_filename: str) -> str:
    new_path_to_filename = "".join(path_to_filename.split(".")[:-1]) + ".avi"
    if not os.path.isfile(new_path_to_filename):
        ff = ffmpy.FFmpeg(
            executable=executable,
            inputs={path_to_filename: None},
            outputs={new_path_to_filename: None},
        )
        ff.run()
    return new_path_to_filename


def main():
    path_to_file = "mts_videos_to_convert_an_join"
    output_path = "avi_output_joined_file"
    output_paths = []
    files_to_convert = os.listdir(path_to_file)
    for file in files_to_convert:
        if file.endswith("MTS"):
            file = os.path.abspath(os.path.join(path_to_file, file))
            new_name = convert_from_mts_to_avi(os.path.abspath(file))
            print("successfully converted {0} to {1}".format(file, new_name))
            output_paths.append(new_name)
    output_file_name = "".join("".join(file.split(".")[0] for file in files_to_convert))

    command_to_join_converted_files = '{0} -i  "concat:{1}" -c copy {2}.avi'.format(
        executable,
        "|".join(f for f in output_paths),
        os.path.join(output_path, output_file_name),
    )

    os.system(command_to_join_converted_files)


if __name__ == "__main__":
    main()
