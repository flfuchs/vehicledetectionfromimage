# Multi-type_vehicles_flow_statistics
According to YOLOv3 and SORT algorithms, counting multi-type vehicles. Implemented by Pytorch.  
Detecting and tracking the vehicles in \["bicycle","bus","car","motorbike","truck"].

## References
- Original code https://github.com/WonJayne/Multi-type_vehicles_flow_statistics
- yolov3-darknet  https://github.com/pjreddie/darknet
- yolov3-pytorch  https://github.com/eriklindernoren/PyTorch-YOLOv3
- sort https://github.com/abewley/sort

## Dependencies
- ubuntu/windows
- python>=3.10
- (cuda for better performance)

## Usage
1. If not already in the repo: Download the pre-trained yolov3 weight file [here](https://pjreddie.com/media/files/yolov3.weights) and put it into `weights` directory;  
2. Create your virtual environment and install all dependencies from requirements.txt (`pip install -r requirements.txt`)
3. If you want to use cuda check [here](https://pytorch.org/get-started/locally/) if there is a pytorch version for you available, install as instructed  
4. Adjust all configuration settings in `config.py` file (i.e. frames to look at, time per frame etc.)
5. Run `python3 app.py`;
6. Select video and double click the image to select area, and then start;
7. After detecting and tracking, the detection results are saved under `results` directory, using the same name as the input file in csv format

