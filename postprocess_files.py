import os
from pathlib import Path

import numpy as np
import pandas as pd
import plotly.express as px


def convert_csv_file(path_to_filename: str, output_folder: str) -> str:
    column_names = [
        "identifier",
        "type",
        "first_detection_time",
        "last_detection_time",
        "current_count",
        "observed_pixel_distance",
    ]

    import_df = pd.read_csv(path_to_filename, sep=";", names=column_names)
    """with open(path_to_filename.replace(".csv", ".txt")) as distance_file:
        observed_distance_from_txt_file = float(distance_file.read())"""
    observed_distance_from_txt_file = 50
    maximal_pixel_distance = np.quantile(import_df["observed_pixel_distance"], 0.75)
    distance_in_meters = import_df["observed_pixel_distance"] / (
        maximal_pixel_distance / observed_distance_from_txt_file
    )
    observed_mean_speed = (
        distance_in_meters / (import_df["last_detection_time"] - import_df["first_detection_time"]) * 3.6
    )
    dt = 50
    last_detection_timestamp = int(max(import_df["last_detection_time"]) / dt) * dt + dt
    time_bins = np.linspace(0, last_detection_timestamp, int((last_detection_timestamp) / dt + 1))
    digitized_last_detection_time = np.digitize(import_df["last_detection_time"], time_bins)

    processed_df = import_df.copy()
    processed_df["observed_distance_in_meters"] = distance_in_meters
    processed_df["observed_mean_speed"] = observed_mean_speed
    processed_df["digitized_last_detection_time"] = digitized_last_detection_time

    # fig = px.scatter(x=processed_df["last_detection_time"].tolist(), y=processed_df["observed_mean_speed"].tolist())
    # fig.show()
    # maximal_pixel_distance = max(import_df["observed_pixel_distance"]) * 0.70
    aggregated_mean_speed = processed_df.groupby("digitized_last_detection_time")["observed_mean_speed"].mean()
    aggregated_observed_flow_normalized = processed_df.groupby("digitized_last_detection_time")[
        "observed_pixel_distance"
    ].sum() / (maximal_pixel_distance)
    aggregated_observed_flow = processed_df.groupby("digitized_last_detection_time")["observed_pixel_distance"].count()
    aggregated_space_density = (
        processed_df.groupby("digitized_last_detection_time")["current_count"].mean() * 8 / distance_in_meters
    )

    aggregated_df = pd.DataFrame()
    aggregated_df["time_interval"] = processed_df["digitized_last_detection_time"].unique() * dt
    aggregated_df["aggregated_mean_speed"] = aggregated_mean_speed
    aggregated_df["aggregated_observed_flow_normalized"] = aggregated_observed_flow_normalized / dt * 3600
    aggregated_df["aggregated_observed_flow"] = aggregated_observed_flow / dt * 3600
    aggregated_df["aggregated_current_count"] = aggregated_space_density

    f_name = Path(path_to_filename).name
    fig = px.scatter(
        x=aggregated_df["time_interval"].tolist(),
        y=aggregated_df["aggregated_mean_speed"].tolist(),
    )
    fig.update_layout(
        title="Time vs. aggregated_mean_speed \n {0}".format(f_name),
        xaxis_title="time [sec]",
        yaxis_title="aggregated_mean_speed in km/h dt: {0} sec".format(dt),
    )
    fig.show()
    fig = px.scatter(
        x=aggregated_df["time_interval"].tolist(),
        y=aggregated_df["aggregated_observed_flow_normalized"].tolist(),
    )
    fig.update_layout(
        title="Time vs. aggregated_observed_flow_normalized\n {0}".format(Path(path_to_filename).name),
        xaxis_title="time [sec]",
        yaxis_title="aggregated_observed_flow_normalized in 1/h dt: {0} sec".format(dt),
    )
    # fig.show()
    fig = px.scatter(
        x=aggregated_df["time_interval"].tolist(),
        y=aggregated_df["aggregated_observed_flow"].tolist(),
    )
    fig.update_layout(
        title="Time vs. aggregated_observed_flow\n {0}".format(Path(path_to_filename).name),
        xaxis_title="time [sec]",
        yaxis_title="aggregated_observed_flow in 1/h dt: {0} sec".format(dt),
    )
    fig.show()
    fig = px.scatter(
        x=aggregated_df["time_interval"].tolist(),
        y=aggregated_df["aggregated_current_count"].tolist(),
    )
    fig.update_layout(
        title="Time vs. aggregated_current_count\n {0}".format(Path(path_to_filename).name),
        xaxis_title="time [sec]",
        yaxis_title="aggregated_normalized_density in 1/h dt: {0} sec".format(dt),
    )
    # fig.show()

    path_to_processed = os.path.join(output_folder, "postprocessed" + Path(path_to_filename).name)
    processed_df.to_csv(path_to_processed, sep=";")
    path_to_aggregated_filename = os.path.join(output_folder, "aggregated" + Path(path_to_filename).name)
    aggregated_df.to_csv(path_to_aggregated_filename, sep=";")

    return path_to_processed


def main():
    path_to_file = "."
    output_path = "post_processed_csv_files"
    output_paths = []
    files_to_convert = os.listdir(path_to_file)
    for file in files_to_convert:
        if file.endswith("csv"):
            file = os.path.abspath(os.path.join(path_to_file, file))
            new_name = convert_csv_file(os.path.abspath(file), os.path.abspath(output_path))
            print("successfully converted {0} to {1}".format(file, new_name))
            output_paths.append(new_name)


if __name__ == "__main__":
    main()
