# coding:utf-8
import os
from collections import defaultdict
from math import sqrt

import cv2
import numpy as np
from PyQt5.QtCore import QThread, pyqtSignal
from tqdm import tqdm

import predict
from config import (
    color_dict,
    NAMES,
    SECONDS_PER_FRAME,
    FRAME_COUNT_STEP,
    UPDATE_DISPLAY_AT_EVERY_FRAME,
    MINIMAL_NUMBER_OF_OBSERVATIONS_PER_DETECTION,
    COUNTS_TO_STOP_TRACKING_AFTER,
)
from utils.sort import KalmanBoxTracker, Sort

TIME_STEP = SECONDS_PER_FRAME * FRAME_COUNT_STEP


class CounterThread(QThread):
    sin_counterResult = pyqtSignal(np.ndarray)
    sin_runningFlag = pyqtSignal(int)
    sin_videoList = pyqtSignal(str)
    sin_countArea = pyqtSignal(list)
    sin_done = pyqtSignal(int)
    sin_counter_results = pyqtSignal(list)
    sin_pauseFlag = pyqtSignal(int)
    save_dir: str = "results"

    def __init__(self, model, class_names, device):
        super(CounterThread, self).__init__()

        self.model = model
        self.class_names = class_names
        self.device = device

        self.permission = set(NAMES)

        self._color_dict = color_dict.copy()

        # create instance of SORT
        self.mot_tracker = Sort(max_age=256, min_hits=4)
        self.count_area = None
        self._running_flag = 0
        self._pause_flag = 0
        self._video_path = []
        self.last_max_id = 0
        self._tracker_history = {}  # save history

        self.sin_runningFlag.connect(self.update_flag)
        self.sin_videoList.connect(self.update_video_name)
        self.sin_countArea.connect(self.update_count_area)
        self.sin_pauseFlag.connect(self.update_pause_flag)

        if not os.path.exists(self.save_dir):
            os.makedirs(self.save_dir)

    def run(self):
        self.last_max_id = 0
        cap = cv2.VideoCapture(self._video_path)
        n_frames = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
        for frame_count in tqdm(range(0, n_frames)):
            if not self._running_flag:
                break
            ret, current_frame = cap.read()
            if frame_count % FRAME_COUNT_STEP != 0:
                continue
            if ret:
                this_frame = self.counter(current_frame, frame_count)
                if UPDATE_DISPLAY_AT_EVERY_FRAME:
                    self.sin_counterResult.emit(this_frame)

        KalmanBoxTracker.count = 0
        cap.release()
        if self._running_flag:
            self.sin_done.emit(1)

    def update_pause_flag(self, flag):
        self._pause_flag = flag

    def update_flag(self, flag: bool) -> None:
        self._running_flag = flag

    def update_video_name(self, video_path: str) -> None:
        self._video_path = video_path

    def update_count_area(self, area):
        print("Update countArea!")
        self.count_area = np.array(area)

    def counter(self, current_frame: np.array, frame_count: int):
        # painting area
        AreaBound = [
            min(self.count_area[:, 0]),
            min(self.count_area[:, 1]),
            max(self.count_area[:, 0]),
            max(self.count_area[:, 1]),
        ]
        painting = np.zeros((AreaBound[3] - AreaBound[1], AreaBound[2] - AreaBound[0]), dtype=np.uint8)
        CountArea_mini = self.count_area - AreaBound[0:2]
        cv2.fillConvexPoly(painting, CountArea_mini, (1,))

        objects = predict.yolo_prediction(self.model, self.device, current_frame, self.class_names)
        objects = filter(lambda x: x[0] in self.permission, objects)
        objects = filter(lambda x: x[1] > 0.5, objects)
        objects = list(
            filter(
                lambda x: point_in_count_area(painting, AreaBound, [int(x[2][0]), int(x[2][1] + x[2][3] / 2)]),
                objects,
            )
        )

        # filter out repeat bbox
        objects = filter_out_repeat(objects)

        detections = [
            [
                int(item[2][0] - item[2][2] / 2),
                int(item[2][1] - item[2][3] / 2),
                int(item[2][0] + item[2][2] / 2),
                int(item[2][1] + item[2][3] / 2),
                item[1],
            ]
            for item in objects
        ]
        track_bbs_ids = self.mot_tracker.update(np.array(detections))

        if UPDATE_DISPLAY_AT_EVERY_FRAME:
            for i in range(len(self.count_area)):
                cv2.line(
                    current_frame,
                    tuple(self.count_area[i]),
                    tuple(self.count_area[(i + 1) % (len(self.count_area))]),
                    (0, 0, 255),
                    2,
                )

        if len(track_bbs_ids) > 0:
            for bb in track_bbs_ids:  # add all bbox to history
                vehicle_id = int(bb[-1])
                object_found = get_obj(bb, objects)
                center_point = (
                    object_found[2][0] + object_found[2][2] * 0.5,
                    object_found[2][1] + object_found[2][3] * 0.5,
                )
                if vehicle_id not in self._tracker_history.keys():  # add new id
                    self._tracker_history[vehicle_id] = {
                        "no_update_count": 0,
                        "his": [object_found[0]],
                        "first_frame": frame_count,
                        "last_frame": frame_count,
                        "last_position_x_y": center_point,
                        "first_position_x_y": center_point,
                    }
                else:
                    self._tracker_history[vehicle_id]["no_update_count"] = 0
                    self._tracker_history[vehicle_id]["his"].append(object_found[0])
                    self._tracker_history[vehicle_id]["last_frame"] = frame_count
                    self._tracker_history[vehicle_id]["last_position_x_y"] = center_point

        if UPDATE_DISPLAY_AT_EVERY_FRAME:
            for item in track_bbs_ids:
                bb = list(map(lambda x: int(x), item))
                vehicle_id = bb[-1]
                x1, y1, x2, y2 = bb[:4]

                his = self._tracker_history[vehicle_id]["his"]
                result = {i: his.count(i) for i in set(his)}
                res = sorted(result.items(), key=lambda d: d[1], reverse=True)
                objectName = res[0][0]

                boxColor = self._color_dict[objectName]
                cv2.rectangle(current_frame, (x1, y1), (x2, y2), boxColor, thickness=2)
                cv2.putText(
                    current_frame,
                    str(vehicle_id) + "_" + objectName,
                    (x1 - 1, y1 - 3),
                    cv2.FONT_HERSHEY_COMPLEX,
                    0.7,
                    boxColor,
                    thickness=2,
                )

        counter_results = []
        removed_id_list = []

        for key, tracked in self._tracker_history.items():  # extract id after tracking
            if tracked["no_update_count"] > COUNTS_TO_STOP_TRACKING_AFTER:
                his = tracked["his"]
                if len(his) >= MINIMAL_NUMBER_OF_OBSERVATIONS_PER_DETECTION:
                    results = defaultdict(int)
                    for vehicle_id in his:
                        results[vehicle_id] += 1
                    most_frequent_class = sorted(
                        results.items(),
                        key=lambda key_value_pair: key_value_pair[1],
                        reverse=True,
                    )[0][0]
                    first_time_stamp = self._tracker_history[key]["first_frame"] * TIME_STEP
                    last_time_stamp = self._tracker_history[key]["last_frame"] * TIME_STEP
                    distance_in_pixels = sqrt(
                        (
                            self._tracker_history[key]["last_position_x_y"][0]
                            - self._tracker_history[key]["first_position_x_y"][0]
                        )
                        ** 2
                        + (
                            self._tracker_history[key]["last_position_x_y"][1]
                            - self._tracker_history[key]["first_position_x_y"][1]
                        )
                        ** 2
                    )
                    counter_results.append(
                        [
                            key,
                            most_frequent_class,
                            first_time_stamp,
                            last_time_stamp,
                            distance_in_pixels,
                        ]
                    )
                # del id
                removed_id_list.append(key)
            else:
                self._tracker_history[key]["no_update_count"] += 1

        for tracked_id in removed_id_list:
            self._tracker_history.pop(tracked_id)

        if len(counter_results) > 0:
            self.sin_counter_results.emit(counter_results)

        return current_frame

    def emit_time_code(self, time_code):
        self.sin_timeCode.emit(time_code)


def filter_out_repeat(objects):
    objects = sorted(objects, key=lambda x: x[1])
    object_count = len(objects)
    new_objects = []
    if object_count > 1:
        for i in range(object_count - 1):
            flag = 0
            for j in range(i + 1, object_count):
                x_i, y_i, w_i, h_i = objects[i][2]
                x_j, y_j, w_j, h_j = objects[j][2]
                box1 = [
                    int(x_i - w_i / 2),
                    int(y_i - h_i / 2),
                    int(x_i + w_i / 2),
                    int(y_i + h_i / 2),
                ]
                box2 = [
                    int(x_j - w_j / 2),
                    int(y_j - h_j / 2),
                    int(x_j + w_j / 2),
                    int(y_j + h_j / 2),
                ]
                if cal_iou(box1, box2) >= 0.7:
                    flag = 1
                    break
            # if no repeat
            if not flag:
                new_objects.append(objects[i])
        # add the last one
        new_objects.append(objects[-1])
    else:
        return objects

    return list(tuple(new_objects))


def cal_iou(box1, box2):
    x1 = max(box1[0], box2[0])
    y1 = max(box1[1], box2[1])
    x2 = min(box1[2], box2[2])
    y2 = min(box1[3], box2[3])
    i = max(0, (x2 - x1)) * max(0, (y2 - y1))
    u = (box1[2] - box1[0]) * (box1[3] - box1[1]) + (box2[2] - box2[0]) * (box2[3] - box2[1]) - i
    return float(i) / float(u)


def get_obj(item, objects):
    iou_list = []
    for i, detected_object in enumerate(objects):
        x, y, w, h = detected_object[2]
        x1, y1, x2, y2 = int(x - w / 2), int(y - h / 2), int(x + w / 2), int(y + h / 2)
        iou_list.append(cal_iou(item[:4], [x1, y1, x2, y2]))
    max_index = iou_list.index(max(iou_list))
    return objects[max_index]


def point_in_count_area(painting, area_boundary, point):
    h, w = painting.shape[:2]
    point = np.array(point)
    point = point - area_boundary[:2]
    if point[0] < 0 or point[1] < 0 or point[0] >= w or point[1] >= h:
        return 0
    else:
        return painting[point[1], point[0]]
