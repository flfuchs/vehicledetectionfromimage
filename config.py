color_dict = {
    "bicycle": (179, 52, 255),
    "bus": (255, 191, 0),
    "car": (127, 255, 0),
    "motorbike": (0, 140, 255),
    "truck": (0, 215, 255),
}

NAMES = tuple(color_dict.keys())
SECONDS_PER_FRAME = 1 / 25
FRAME_COUNT_STEP = 2
UPDATE_DISPLAY_AT_EVERY_FRAME = True
MINIMAL_NUMBER_OF_OBSERVATIONS_PER_DETECTION = 5
COUNTS_TO_STOP_TRACKING_AFTER = 10
